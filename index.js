const express = require('express')
const app = express()
const jwt = require('jsonwebtoken');
const mongoose = require("mongoose");
const parseJson = require('parse-json');

var kafka = require('kafka-node')
const router = require('./routes/routes');
const authorize = require("./authorization-middleware");
const config = require("./config/config");
const UserData = require('./modal/UserData');
const controller = require('./kafka/Producer.js')

var port = 4000

const bp = require('body-parser')
app.use(bp.json())
app.use(bp.urlencoded({ extended: true }))

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

// mongoose.connect(uri, { useNewUrlParser: true, useUnifiedTopology: true }).then(...)

// MongoClient.connect(url, function(err, db) {
//   if (err) throw err;
//   var dbo = db.db("BTPN_Database");
//   dbo.createCollection("UserData", function(err, res) {
//     if (err) throw err;
//     console.log("Collection created!");
//     db.close();
//   });
// });

mongoose.connect(url, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
  useFindAndModify: false,
});

app.get("/token", (req, res) => {
  const payload = {
    name: "DimasFahmiSuntoro - BTPN_PROJECT",
    scopes: "data:important"
  };
  const token = jwt.sign(payload, config.JWT_SECRET);
  res.send(token);
});

app.get('/read', async (req, res) => {
  try {      
      MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("BTPN_Database");
        dbo.collection("UserData").find({}).toArray(function(err, result) {
          if (err) throw err;           
          res.send(result);
          db.close();
        });        
      });      
    } catch (error) {
     res.send('Error');
    }
  })

  
app.post('/create', authorize("data:important"), async (req, res) => {
  MongoClient.connect(url, function(err, db) {
      if (err) throw err;
      var dbo = db.db("BTPN_Database");
      var myobj = req.body;      
      dbo.collection("UserData").insertOne(myobj, function(err, res) {
        if (err) throw err;
        console.log("1 document inserted");
        db.close();
      });
    });
    res.send('Create Success');
})

app.put('/update/:id', authorize("data:important"), async (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("BTPN_Database");
    var myobj = req.body;
    var myquery = { id: myobj.id };
    var newvalues = { $set: {userName: myobj.userName, accountName: myobj.accountName, 
      accountEmail: myobj.accountEmail, identityNumber: myobj.identityNumber } };
    dbo.collection("UserData").updateOne(myquery, newvalues, function(err, res) {
      if (err) throw err;
      console.log("1 document updated");
      db.close();
    });
  });
  res.send('Update Success');
})

app.delete('/delete/:id', authorize("data:important"), async (req, res) => {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db("BTPN_Database");
    var myobj = req.body.id;
    var myquery = { id: myobj };
    // var newvalues = { $set: {name: myobj.getname, address: myobj.acc } };
    dbo.collection("UserData").deleteOne(myquery, function(err, obj) {
      if (err) throw err;
      console.log(obj.result.n + " document(s) deleted");
      db.close();
    });
  });
  res.send('Delete Success');
})

app.listen(port, 
    function() { 
      console.log(`Simple Express app listening on port ${port}!`);
      console.log(`Kafka producer running at ${port}!`);
})

app.use('/', router)

module.exports = app