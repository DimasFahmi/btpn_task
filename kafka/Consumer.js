var kafka = require('kafka-node');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
const mongoose = require("mongoose");

const createConsumer = async (req, res, next) => {    
    var topic = req.params.topic;
    Consumer = kafka.Consumer,
    client = new kafka.KafkaClient(),
    consumer = new Consumer(client,
        [{ topic: topic, offset: 0}],
        {
            autoCommit: true
        }
    );

    MongoClient.connect(url, function(err, db) {
        if (err) throw err;
        var dbo = db.db("BTPN_Database");
        console.log(req.body.accountName);
        dbo.collection("UserData").find({ accountName: {$eq : req.body.accountName}}).toArray(function(err, result) {
          if (err) throw err;
          console.log(result);
          var fixMessage = result[0]
          db.close();
          res.send({
            message:fixMessage,
            topic:topic
        })
        });
      });

    // res.send({
    //     message:"Successfullu Get Message From Producer",
    //     topic:topic
    // })

    consumer.on('message', function (message) {        
        console.log(message);
    });
       
    consumer.on('error', function (err) {
        console.log('Error:',err);
    })
    
    consumer.on('offsetOutOfRange', function (err) {
        console.log('offsetOutOfRange:',err);
    })
}

module.exports = { createConsumer}