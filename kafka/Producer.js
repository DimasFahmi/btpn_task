var kafka = require('kafka-node');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
const mongoose = require("mongoose");

var Producer = kafka.Producer,
    client = new kafka.KafkaClient(),
    producer = new Producer(client);

producer.on('ready', function () {
    console.log('Producer is ready');
});

producer.on('error', function (err) {
    console.log('Producer is in error state');
    console.log(err);
})

const createProducer = async (req, res, next) => {
    var sentMessage = JSON.stringify(req.body.message);
   
    payloads = [
        { topic: req.body.topic, messages:sentMessage , partition: 0 }
    ];

    producer.send(payloads, function (err, data) {
            // res.json(data);
    });
    res.send({
        message: "Succesfuly Sending Message",
        payloads:payloads
    })
}

module.exports = { createProducer}