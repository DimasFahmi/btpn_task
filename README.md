# BTPN_TASK

Untuk Task BTPN


Untuk CRUD
* Create, Delete dan Update harus ada header dengan nama authorization
	dan isinya Bearer Token, Link = http://localhost:4000/create Post Method
	dan Request Body
	{
		"userName": "Dimas Fahmi Suntoro, A.Md Menuju S.T Rencana M.T",
		"accountName": "DIMAS_GTA_PS2_Liberty_City",
		"accountEmail":"dimas@fahmi.suntoro.co.id",
		"identityNumber":"1237467588476665"
	}
	
* Token diisi di Link = http://localhost:4000/token Get Method

* Untuk Update dan Delete 
	Update http://localhost:4000/update/id
	Delete  http://localhost:4000/delete/id

* Untuk Read ada di Link = http://localhost:4000/read tanpa token

Dari sana terdapat aplkasi Monolitik yang mana terdapat CRUD Project dan ditambahin feature Producer dan Consumer dengan Kafka 

Ada di link 
http://localhost:4000/producer untuk Producer 
dengan Request Body 
{
    "topic": "BTPN",
    "message": {
        "userName": "Dimas Fahmi Suntoro .A.Md Menuju S.T Rencana M.T",
        "accountName": "Teknik Elektro Tenaga Listrik dan Elektro Teknologi Informasi",
        "accountEmail": "dimas.fahmi.suntoro.co.id@yahoo.co.my",
        "identityNumber": "1237467588476665"
    }
}

dan http://localhost:4000/BTPN atau http://localhost:4000/topic
untuk Consumer, data Consumer ditampilkan di console


