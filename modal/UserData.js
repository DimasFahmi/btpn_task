const mongoose = require("mongoose");
// Buat Schema
const UserData = mongoose.Schema({
    id: {
        type: Number,
        required: true,
      },
      userName: {
        type: String,
        required: true,
      },
      accountName: {
        type: String,
        required: true,
      },
      accountEmail: {
        type: String,
        required: true,
      },
      identityNumber: {
        type: String,
        required: true,
      }
});

module.exports = mongoose.model('UserData', UserData);